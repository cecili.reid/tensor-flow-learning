# Handwritten digit recognition with TensorFlow.js (folder digital-recognition)
Tutorial followed as referenced: https://codelabs.developers.google.com/codelabs/tfjs-training-classfication/index.html#0

Steps:
1. Create react application `npx create-react-app digital-recognition`. This is done so we can use the server and to also start learning more about how to implement Tensorflow learning models with a react front end.
2. Completed step 2 of the tutorial guide except placed the index.html in the public folder of the react application and placed the javascript files (data and script) in the same folder. Also commented out the reactDom render command in index.js.

3. Update script.js to load data and show image examples.

4. Define the model architecture.

5. Train the model and display metrics during training.

## Takeways
This approach is entirely in browser and does not involve node.js. There are opportunities to practice adding minimum probability required or storing a trained model.
