# Tensor Flow Learning

Repository to hold all projects related to learning more about TensorFlow.

## Description of projects

| Folder Name | Description | Notes |
| ----------- | ----------- | ----- |
| digit-recognition | In browser tutorial of using mnist | https://codelabs.developers.google.com/codelabs/tfjs-training-classfication/index.html |
| digit-recognition-node | Node.js example of training a model with MNIST data set | https://github.com/tensorflow/tfjs-examples/tree/master/mnist-node |
| pretrained-mobilenet | Use a pretrained model of mobilenet to identify images and transfer learning | https://codelabs.developers.google.com/codelabs/tensorflowjs-teachablemachine-codelab/index.html#0 |


## Resources
https://github.com/tensorflow/tfjs-examples/tree/master/mnist-node
https://codelabs.developers.google.com/codelabs/tfjs-training-classfication/index.html#1
https://www.tensorflow.org/js/guide/node_in_cloud#deploy_nodejs_project_on_heroku
https://github.com/tensorflow/tfjs-models
https://observablehq.com/@nsthorat/how-to-build-a-teachable-machine-with-tensorflow-js
